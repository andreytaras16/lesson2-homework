#!/bin/bash

sleep 30
# install docker
sudo yum update
sudo yum install -y docker
sudo usermod -a -G docker ec2-user
sudo service docker start
sudo chkconfig docker on

# install docker-compose
sudo curl -L https://github.com/docker/compose/releases/download/1.21.0/docker-compose-`uname -s`-`uname -m` | sudo tee /usr/local/bin/docker-compose > /dev/null
sudo chmod +x /usr/local/bin/docker-compose
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
docker-compose --version

# install 
sudo yum -y install git

# clone project 
git clone https://gitlab.com/andreytaras16/lesson2-homework.git
cd lesson2-homework

# start project
sudo docker-compose build